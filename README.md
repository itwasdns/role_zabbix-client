role_zabbix-client
================

A simple role to install and configure Zabbix clients

Conventions
-----------

An explanation of conventions used in roles can be found in the [docs](./docs).

* [Coding Conventions](./docs/CONVENTIONS_ROLE.md)

Requirements
------------

* [virtualenv](https://virtualenv.pypa.io/en/stable)
* [zabbix-api](https://pypi.org/project/zabbix-api/)


Installation
------------

Install from [GitHub](https://github.com/llamalump/role_zabbix-client)
```bash
git clone https://github.com/llamalump/role_zabbix-client.git
```

Usage
-----

### Install Ansible

The recommended and portable way to install [Ansible](https://docs.ansible.com/)
is via [virtualenv](https://virtualenv.pypa.io/en/stable/).

```bash
# Create a new virtualenv
virtualenv $env_name
# Activate virtualenv
source $env_name/bin/activate
# Install Ansible
pip install ansible
```

License
-------

MIT

Author
------

* llamalump <llamalump@itwasdns.com>
